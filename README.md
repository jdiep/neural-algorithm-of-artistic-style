# Neural Algorithm of Artistic Style

This GitLab repository contains the code for the "Neural Algorithm of Artistic Style" project. It was investigated as a side project. 

## Summary
This is a Docker implementation of the paper "Neural Algorithm of Artistic Style" by Leon Gatys, Alexander Ecker and Matthias Betghe. An artificial neural network was introduced in order to generate an image which combines the content and the style of two different images respectively.

## Methods

The authors suggested using the VGG architecture by Karen Simonyan and Andrew Zisserman as the basis for the feature encoding task. Specifically in this implementation, the VGG-19 variant was used, where the neural network consists of 16 convolutional, 5 pooling and subsequent fully connected layers. Here, the latter has no specific relevance for the image generation task. Each hidden layer represents a different encoding of the input image, which are used to describe its specific characteristics. Given two images (a photograph and an artwork) representing content and style respectively as well as an appropriate cost function consisting of a content and style term, the pixel values of an initial white noise image can be determined via an optimization process until the resulting image captures both characteristics to a certain degree.

## Installation

* [Docker 19.03.8](https://www.docker.com/get-started)

## Starting Instructions

Navigate to the docker folder and execute the following command in order to build the Docker image: 

```console
docker build -t style-ubuntu .
```

Run the built image with the following command and navigate to the home folder:

```console
docker run -it style-ubuntu
```

Navigate to the home folder and clone this repository:

```console
git clone https://gitlab.com/jdiep/neural-algorithm-of-artistic-style.git
```

Navigate to the cloned pretrained-model folder and load the VGG19 model:

```console
wget https://www.vlfeat.org/matconvnet/models/imagenet-vgg-verydeep-19.mat
```

After running the program, use the following command on your host machine in order to obtain the results:

```console
docker cp <container_name>:/home/neural-algorithm-of-artistic-style/source/output .
```

More useful Docker commands can be found on: 

* [https://gitlab.com/jdiep/semester-thesis/-/blob/master/docker/README.md](https://gitlab.com/jdiep/semester-thesis/-/blob/master/docker/README.md)

## Result

![Results](https://i.imgur.com/jUYaVbV.png)

## Remarks

* According to the paper, replacing the max-pooling operations in the VGG19 neural architecture by average-pooling results in improvement of the gradient flow and more appealing results.
* Better results might be achievable by training longer and using a better optimization algorithm.
* The weight ratio between the content and style image determines the degree of the artistic effect on the generated image. Here, a weight ratio of 0.25 was set. 
* As described in the paper, "conv1_1", "conv2_1", "conv3_1", "conv4_1" and "conv5_1" were used to calculate the style cost. Thereby, all layers were weighted equally.
* A coarse-to-sparse approach can speed up computational time. The idea is to bring the initial guess as fast as possible to the optimal result by starting with images at low resolutions. Due to lower information density, processing needs much less time and the resulting generated image comes closer to the optimum. Subsequently, the next iteration starts at higher resolutions whereby the last generated image is used as the initialization.

## Credential

* The core of this work is based on the publication ["Neural Algorithm of Artistic Style"](https://arxiv.org/pdf/1508.06576.pdf) by Leon Gatys, Alexander Ecker and Matthias Bethge. 
* The coding is from the deep learning specialization course ["Convolutional Neural Networks"](https://www.coursera.org/learn/convolutional-neural-networks) by Andrew Ng, Kian Katanforoosh and Younes Mourri. 

## Version

* 8. May 2020: Version 1.0

## Author

* Johann Diep (MSc ETH in Mechanical Engineering, johanndiep@gmail.com)
